//
//  Course: CS 59000-01
//  Name: Chris Dzialowy
//  Assignment: 1
//
//  Prints to standard output a range of lines from a text file or
//  from standard input. If there are not enough lines of input, the program
//  should print what it can.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char** argv)
{
	if (argc > 2 && atoi(argv[1]) <= atoi(argv[2]) && atoi(argv[1]) > 0
	                                               && atoi(argv[2]) > 0)
	{
		FILE* fptr;
		fptr = (argc > 3) ? fopen(argv[3],"r") : stdin;

		if (fptr == NULL)
		{
			perror("Failed to open stream");
		}

		else
		{
			size_t linesiz = 0;
			char* line = NULL;
			int i;

			for (i=1; i <= atoi(argv[2]); i++)
		    {
				if (getline(&line, &linesiz, fptr) == -1)
				{
					break;
				}

				else if (i >= atoi(argv[1]))
				{
					printf("%s",line);
				}
			}

		fclose(fptr);
		free(line);
		}
	}

	else
	{
		printf("Incorrect format: range lower_bound upper_bound [filename]\n");
	}

	return 0;
}//main()
