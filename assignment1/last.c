/*
  Course: CS 59000-01
  Name: Chris Dzialowy
  Assignment: 1

  Prints to standard output the last ten lines of a text file. or
  from standard input. If there are not enough lines of input, the program
  should print what it can.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char** argv)
{
	FILE* fptr;
	fptr = (argc > 1) ? fopen(argv[1],"r") : stdin;

	if(fptr == NULL)
	{
		perror("Failed to open file");
	}

	else
	{
		size_t linesiz = 0;
		char* line[10] = {NULL};
		int i = 0;
		int j = 0;

		while (getline(&line[i%10], &linesiz, fptr) != -1)
		{
			j = i%10;
			i++;
		}

		for (i=j+1; i%10 != j; i++)
		{
		  if (line[i%10] != NULL)
			{
				printf("%s",line[i%10]);
			}
		}

		printf("%s", line[j]);
		fclose(fptr);
	}


	return 0;
}//main()
